<?php
include_once '_debut.inc.php';
include_once '_gestionBase.inc.php';
?>

<!-- Une div contenant la class "container" préfixe obligatoirement les lignes (div de class=row) -->
<div class="container">
    <!-- ligne principale -->
    <div class="row "> 
        <!-- première colonne (s'étend sur de 3 colonnes sur 12 possibles) -->
        <div class="col-md-3 border">
            <br />
            <div id="menuGauche" class="btn-group-vertical btn-block">

                <a href="consultationGroupe.php" class="btn btn-primary ">
                    CONSULTER</a>
                <a href="creationGroupe.php" class="btn btn-primary  ">
                    AJOUTER</a>

                <a href="#" class="btn btn-primary btn-block">
                    RECHERCHER</a>
            </div> 
            <img src="img/clefmusique.gif" class="img-responsive" alt="Responsive image">
        </div>
         <!-- deuxième colonne (s'étend sur 7 colonnes sur 12 possibles à partir de la 3) -->
        <div class="col-md-7 border">
            <br />
            <!-- une ligne dans une colonne -->
            <div class="row">
                <?php 
                $listeGroupe = lireGroupes();
                if($listeGroupe != false):
                    foreach($listeGroupe as $groupe):
                   ?>
                <div class="col-md-6">
                        <article class="panel panel-default articleEtablissement bgColorTheme">
                                <p> Nom :   <?php echo $groupe["nom"] ?></p>
                                <p> Adresse :  <?php echo $groupe["adressePostale"] ?></p>
                            <p> Pays :  <?php echo $groupe["nomPays"] ?></p>
                            <ol class="breadcrumb">
                                <li> 
                                    <a href="modificationGroupe.php?numGroupe=<?php echo $groupe["id"]?>">modifier
                                    </a>
                                </li>
                                <li> 
                                    <a href="detailGroupe.php?numGroupe=<?php echo $groupe["id"]?>">Détail
                                    </a>
                                </li>
                                <li class="active">
                                    <a href="suppressionGroupe.php?numGroupe=<?php echo $groupe["id"]?>">Suppression
                                    </a>
                                </li>
                            </ol>
                        </article>
                    </div>

                <?php endforeach; ?>
               <?php endif; ?>



            </div>
        </div>
    </div>
    <hr>

    <footer>
        <p>&copy; Jules Ferry 2015</p>
    </footer>
</div> <!-- /container -->




<?php include("_fin.inc.php"); ?>
