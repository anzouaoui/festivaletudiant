<?php
include_once '_debut.inc.php';
include_once '_gestionBase.inc.php';

if (isset($_REQUEST)) {
    $id = $_REQUEST['id'];
    $nom = $_REQUEST['nom'];
    $identiteResponsable = $_REQUEST['identiteResponsable'];
    $adressePostale = $_REQUEST['adressePostale'];
    $nombrePersonnes = $_REQUEST['nombrePersonnes'];
    $nomPays = $_REQUEST['nomPays'];
    $hebergement = $_REQUEST['hebergement'];    
    
    modifierGroupe($id, $nom, $identiteResponsable, $adressePostale, $nombrePersonnes, $nomPays, $hebergement);

    header("location: modificationGroupe.php?numGroupe=$id");
} else {
    header("location: consultationGroupe.php");
}
?>