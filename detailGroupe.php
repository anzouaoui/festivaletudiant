<?php
include_once '_debut.inc.php';
include_once '_gestionBase.inc.php';

if(!isset($_REQUEST["numGroupe"])){
    header("location: consultationGroupe.php");
}

$id=$_REQUEST['numGroupe'];

$groupe= obtenirDetailGroupe($id);

if($groupe!=false){
    $id=$groupe['id'];
    $nom=$groupe['nom'];
    $identiteResponsable=$groupe['identiteResponsable'];
    $adressePostale=$groupe['adressePostale'];
    $nombrePersonnes=$groupe['nombrePersonne'];
    $nomPays=$groupe['nomPays'];
    $hebergement=$groupe['hebergement'];
}
?>

<div class="container">
    <div class="row ">
        <div class="col-md-3 border">
            <div id="menuGauche" class="btn-group-vertical btn-block">

                <a href="consultationGroupe.php" class="btn btn-primary ">
                    CONSULTER</a>
                <a href="creationGroupe.php" class="btn btn-primary  ">
                    AJOUTER</a>

                <a href="#" class="btn btn-primary btn-block">
                    RECHERCHER</a>
            </div> 
            <img src="img/clefmusique.gif" class="img-responsive" alt="Responsive image">
        </div>
        <div class="col-md-7 border">
            <article>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo $nom; ?> </h3>
                    </div>
                    <div class="panel-body">
                        <p><?php echo $adressePostale; ?></p>
                        <p><?php echo $nomPays; ?></p>
                        <p><?php echo $identiteResponsable; ?></p>
                        <p>
                              <?php echo $identiteResponsable; ?>
                        </p>
                        <p><?php echo $nombrePersonnes; ?>personnes </p>
                        <p>
                            <?php if($hebergement == 'O'): ?>
                                    hébergé
                                <?php else: ?> 
                                    non hébergé
                            <?php endif; ?>
                        </p>
                </div> 
            </article>
        </div>
    </div>
    <hr>

    <footer>
        <p>&copy; Company 2014</p>
    </footer>
</div> <!-- /container -->







<?php include("_fin.inc.php"); ?>