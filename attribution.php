<?php
include_once '_debut.inc.php';
include_once '_gestionBase.inc.php';
?>

<div class="container">
    <div class="row ">
        <div class="col-md-3 border">
            <div id="menuGauche" class="btn-group-vertical btn-block">

                <a href="consultationEtablissements.php" class="btn btn-primary ">
                    CONSULTER</a>
                <a href="creationEtablissement.php" class="btn btn-primary  ">
                    AJOUTER</a>

                <a href="#" class="btn btn-primary btn-block">
                    RECHERCHER</a>
                <a href="attribution.php" class="btn btn-primary btn-block">
                    ATTRIBUTION</a>
            </div> 
            <img src="img/clefmusique.gif" class="img-responsive" alt="Responsive image">
        </div>
        <div class="col-md-7 border">
            <?php
            $listeEtablissements = listeEtablissement();
            if ($listeEtablissements != false):
                foreach ($listeEtablissements as $etablissement):
                    ?>
                    <div class="col-md-7 border">
                        <article>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><?php echo $etablissement["nom"]; ?> </h3>
                                </div>
                                <div class="panel-body">
                                    <?php
                                    $collectionAttribution = listeAttribution();
                                    if ($collectionAttribution != false):
                                        foreach ($collectionAttribution as $attribution):
                                            ?>
                                            <p>Noms groupe: <?php echo $attribution["nomGroupe"]; ?></p>
                                            <p>Nombres de chambres prêtées: <?php echo $attribution["nombreChambres"]; ?></p>
                                            <br />
                                            <br />
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif; ?> 
                            </div> 
                        </article>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<hr>

<footer>
    <p>&copy; Company 2014</p>
</footer>
</div> <!-- /container -->

<?php include("_fin.inc.php"); ?>



