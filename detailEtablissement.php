<?php
include_once '_debut.inc.php';
include_once '_gestionBase.inc.php';

if(!isset($_REQUEST["numEtablissement"])){
    header("location: consultationEtablissement.php");
}

$id=$_REQUEST['numEtablissement'];

$etablissement=  obtenirDetailEtablissement($id);

if($etablissement!=false){
    $nom=$etablissement['nom'];
    $adresseRue=$etablissement['adresseRue'];
    $codePostal=$etablissement['codePostal'];
    $ville=$etablissement['ville'];
    $tel=$etablissement['tel'];
    $adresseElectronique=$etablissement['adresseElectronique'];
    $type=$etablissement['type'];
    $civiliteResponsable=$etablissement['civiliteResponsable'];
    $nomResponsable=$etablissement['nomResponsable'];
    $prenomResponsable=$etablissement['prenomResponsable'];
    $nombreChambresOffertes=$etablissement['nombreChambresOffertes'];
}
?>

<div class="container">
    <div class="row ">
        <div class="col-md-3 border">
            <div id="menuGauche" class="btn-group-vertical btn-block">

                <a href="consultationEtablissements.php" class="btn btn-primary ">
                    CONSULTER</a>
                <a href="creationEtablissement.php" class="btn btn-primary  ">
                    AJOUTER</a>

                <a href="#" class="btn btn-primary btn-block">
                    RECHERCHER</a>
                <a href="attribution.php" class="btn btn-primary btn-block">
                    ATTRIBUTION</a>
            </div> 
            <img src="img/clefmusique.gif" class="img-responsive" alt="Responsive image">
        </div>

        <div class="col-md-7 border">
            <article>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo $nom; ?> </h3>
                    </div>
                    <div class="panel-body">
                        <p><?php echo $adresseRue; ?></p>
                        <p><?php echo $codePostal; ?></p>
                        <p>
                            <span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span>
                            <?php echo $tel; ?>    
                        </p>
                        <address><?php echo $adresseElectronique; ?></address>
                        <p>
                            <?php if($type ==1): ?>
                                    Etablissement scolaire 
                                <?php else: ?> 
                                    Autre établissement
                            <?php endif; ?>
                        </p>
                        <p>
                              <?php echo $civiliteResponsable. " ".$prenomResponsable." ".$nomResponsable; ?>
                        </p>
                        <p><?php echo $nombreChambresOffertes; ?>chambre(s)proposées </p>
                    </div>
                </div> 
            </article>
        </div>
    </div>
    <hr>

    <footer>
        <p>&copy; Company 2014</p>
    </footer>
</div> <!-- /container -->







<?php include("_fin.inc.php"); ?>