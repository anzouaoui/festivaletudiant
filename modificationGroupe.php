<?php
    include_once '_debut.inc.php'; 
    include_once '_gestionBase.inc.php'; 
    
    if(isset($_GET["numGroupe"])){
    $id = $_GET["numGroupe"];
    $groupe = obtenirDetailGroupe($id);
    
    $nom = $groupe['nom'];
    $identiteResponsable = $groupe['identiteResponsable'];
    $adressePostale = $groupe['adressePostale'];
    $nombrePersonnes = $groupe['nombrePersonnes'];
    $nomPays = $groupe['nomPays'];
    $hebergement = $groupe['hebergement'];
}
    ?>

<form method='post' action='creationGroupe.traitement.php'>
    <div class="container">
        <div class="row ">
            <div class="col-md-3 border">
                <br />
                <div id="menuGauche" class="btn-group-vertical btn-block">

                    <a href="consultationGroupe.php" class="btn btn-primary ">
                        CONSULTER</a>
                    <a href="creationGroupe.php" class="btn btn-primary  ">
                        AJOUTER</a>

                    <a href="#" class="btn btn-primary btn-block">
                        RECHERCHER</a>
                </div> 
                <img src="img/clefmusique.gif" class="img-responsive" alt="Responsive image">
            </div>

            <!-- Entre deux layout -->
            <div class="col-md-1 border">

            </div>

            <!-- Layout Droit -->
            <div class="col-md-8 border">
                <br />
                <article>
                    <!-- ligne ID ETAB -->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group">

                                <span class="input-group-addon minTextBox bgColorTheme" >Identifiant</span>
                                <input type="text"  class="form-control" value="<?php echo $id; ?>" name="id" id="id"  maxlength='8'   pattern="^[a-zA-Z0-9]{3,8}$" title="Saisir 3 caractères au minimum"  required>
                            </div>
                        </div>
                    </div>
                    <!-- /ligne -->
                    
                    <br />

                    <!-- ligne NOM ETAB -->
                    <div class="row">
                        <div class="col-md-5">
                            <div class="input-group">
                                <span class="input-group-addon bgColorTheme minTextBox">Nom</span>
                                <input type="text" class="form-control" name="nom" value="<?php echo $nom; ?>" id="nom" size="50" 
                                       maxlength="45" pattern="^[-'çéèùa-zA-Z\s]{1,45}$" title="Saisir 1 caractères au minimum" required>
                            </div>
                        </div>
                    </div>
                    <!-- /ligne -->
                    <br/>
                    
                    <!-- ligne ADRESSE -->
                    <div class="row">
                        <div class="col-md-5">
                            <div class="input-group ">
                                <span class="input-group-addon bgColorTheme minTextBox">Responsable</span>
                                <input type="text" name="identiteResponsable" value="<?php echo $identiteResponsable; ?>" id="identiteResponsable" class="form-control"  pattern="^[-,'°çéèù0-9a-zA-Z\s]{3,45}$" title="Saisir une adresse valide : 3 caractères au minimum" maxlength="45" required>

                            </div>
                        </div>
                    </div>
                    <!-- /ligne -->
                    <br />
                                       
                    <!-- ligne ADRESSE -->
                    <div class="row">
                        <div class="col-md-5">
                            <div class="input-group ">
                                <span class="input-group-addon bgColorTheme minTextBox">Adresse</span>
                                <input type="text" name="adressePostale" value="<?php echo $adressePostale; ?>" id="adressePostale" class="form-control"  pattern="^[-,'°çéèù0-9a-zA-Z\s]{3,45}$" title="Saisir une adresse valide : 3 caractères au minimum" maxlength="45" required>

                            </div>
                        </div>
                    </div>
                    <!-- /ligne -->
                    <br />
                    
                    <!-- ligne VILLE  -->
                    <div class="row">
                        <div class="col-md-7">

                            <div class="input-group">
                                <span class="input-group-addon bgColorTheme minTextBox">
                                    Pays
                                </span>
                                <input type="text" class="form-control" name="nomPays" value="<?php echo $nomPays; ?>" id="nomPays" title="Nom du pays" maxlength="35" pattern="^[-'çéèùa-zA-Z\s]{1,35}$" required />
                            </div>
                        </div>
                    </div>
                    <!-- /ligne -->
                    <br/>
                    
                    <div class="row">
                        <div class="col-md-5">
                            <div class="radio-inline">
                                <input type='radio' name='hebergement' id="hebergement" value='1' 
                                        />  
                                <label for="etablissementScolaire">Hébergé</label>
                            </div>
                            <div class="radio-inline">
                                <input type='radio' name='hebergement' id="nonHeberge" value='0' 
                                      />  
                                <label for="autre">Non hébergé</label>
                            </div>
                        </div>
                    </div>
                    <br />
                    
                    <!-- Zone de validation -->
                    <div class="row">
                        <div class="col-lg-2">  
                            <input class="btn btn-primary btn-lg " type="submit" value="Valider">
                        </div>
                        <div class="col-lg-2">
                            <input class="btn btn-primary btn-lg " type="reset" value="Annuler">
                        </div>
                    </div>
                    
                    
