<?php

/**
 * _gestionBase.inc
 * 
 * PHP version 5
 *
 * Ce fichier regroupe les fonctions de gestion de la base de données "festival"
 * @author Pv
 * @version 1.0
 * @package Festival
 */

/**
 * Retourne un gestionnaire de connexion.
 *
 * Se connecte à la base de données "festival" du serveur de bases de données MYSQL et retourne un gestionnaire de connexion
 * 
 * @return PDO|false Un objet PDO en cas de succès, "false" en cas d'echec
 */
function gestionnaireDeConnexion() {
    $pdo = null;
    try {
        $pdo = new PDO(
                'mysql:host=localhost;dbname=festival', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
        );
    } catch (PDOException $err) {
        $messageErreur = $err->getMessage();
        error_log($messageErreur, 0);
    }
    return $pdo;
}

/* Déclaration de la fonction listeEtablissement qui retourne la liste des établissements de la table « ETABLISSEMENT ». */

function listeEtablissement() {
    $resultat = false;
    $pdo = gestionnaireDeConnexion(); /* Objet de connection */
    if ($pdo != false) {
        $req = "select * from etablissement order by id";
        $resultat = $pdo->query($req);
    }
    return $resultat;
}

/* Déclaration de la fonction creerEtablissement qui permet l'ajoute un établissement */

function creerEtablissement($id, $nom, $adresseRue, $codePostal, $ville, $tel, $adresseElectronique, $type, $civiliteResponsable, $nomResponsable, $prenomResponsable, $nombreChambresOffertes) {
    $reussi = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $id = $pdo->quote($id);
        $nom = $pdo->quote($nom);
        $adresseRue = $pdo->quote($adresseRue);
        $codePostal = $pdo->quote($codePostal);
        $ville = $pdo->quote($ville);
        $tel = $pdo->quote($tel);
        $adresseElectronique = $pdo->quote($adresseElectronique);
        $type = $pdo->quote($type);
        $civiliteResponsable = $pdo->quote($civiliteResponsable);
        $nomResponsable = $pdo->quote($nomResponsable);
        $prenomResponsable = $pdo->quote($prenomResponsable);
        $nombreChambresOffertes = $pdo->quote($nombreChambresOffertes);

        $req = "insert into Etablissement values($id,$nom,$adresseRue,$codePostal,$ville,$tel,$adresseElectronique,$type,$civiliteResponsable, $nomResponsable, $prenomResponsable,$nombreChambresOffertes)";
        $resultat = $pdo->exec($req);
        if ($resultat == 1) {
            $reussi = true;
        }
    }
    return $reussi;
}

/* Fin de la fonction */

/* Fonction pour obtenir des détails sur les établissements */

function obtenirDetailEtablissement($id) {
    $detailEtablissement = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $id = $pdo->quote($id);
        $req = "Select * from Etablissement where id=$id";
        $resultat = $pdo->query($req);
        $detailEtablissement = $resultat->fetch();
    }
    return $detailEtablissement;
}

/* Fonction pour modifier les détails sur les établissements */

function modifierEtablissement($id, $nom, $adresseRue, $codePostal, $ville, $tel, $adresseElectronique, $type, $civiliteResponsable, $nomResponsable, $prenomResponsable, $nombreChambresOffertes) {
    $modification = false;
    $pdo = gestionnaireDeConnexion();

    if ($pdo != false) {
        $id = $pdo->quote($id);
        $nom = $pdo->quote($nom);
        $adresseRue = $pdo->quote($adresseRue);
        $codePostal = $pdo->quote($codePostal);
        $ville = $pdo->quote($ville);
        $tel = $pdo->quote($tel);
        $adresseElectronique = $pdo->quote($adresseElectronique);
        $type = $pdo->quote($type);
        $civiliteResponsable = $pdo->quote($civiliteResponsable);
        $nomResponsable = $pdo->quote($nomResponsable);
        $prenomResponsable = $pdo->quote($prenomResponsable);
        $nombreChambresOffertes = $pdo->quote($nombreChambreOffertes);

        $req = "update Etablissement set nom=$nom, adresseRue=$adresseRue, codePostal=$codePostal,"
                . "ville=$ville, tel=$tel, adresseElectronique=$adresseElectronique, type=$type,"
                . "civiliteResponsable=$civiliteResponsable, nomResponsable=$nomResponsable, prenomResponsable=$prenomResponsable,"
                . "nombreChambresOffertes=$nombreChambresOffertes where id= $id";

        $resultat = $pdo->exec($req);
        if ($resultat == 1) {
            $modification = true;
        }
    }
    return $modification;
}

/* Fonction pour consulter les groupes de musique */

function lireGroupes() {
    $resultat = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "select * from groupe order by id";
        $resultat = $pdo->query($req);
    }
    return $resultat;
}

/* Fonction pour ajouter un groupe */

function creerGroupe($id, $nom, $identiteResponsable, $adressePostale, $nombrePersonnes, $nomPays, $hebergement) {
    $reussi = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $id = $pdo->quote($id);
        $nom = $pdo->quote($nom);
        $identiteResponsable = $pdo->quote($identiteResponsable);
        $adressePostale = $pdo->quote($adressePostale);
        $nombrePersonnes = $pdo->quote($nombrePersonnes);
        $nomPays = $pdo->quote($nomPays);
        $hebergement = $pdo->quote($hebergement);

        $req = "insert into Groupe values($id, $nom, $identiteResponsable, $adressePostale, $nombrePersonnes, $nomPays, $hebergement)";
        $resultat = $pdo->exec($req);
        if ($resultat == 1) {
            $reussi = true;
        }
    }
}

/* Fonction pour obtenir des détails sur les groupes */

function obtenirDetailGroupe($id) {
    $detailGroupe = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $id = $pdo->quote($id);
        $req = "Select * from Groupe where id=$id";
        $resultat = $pdo->query($req);
        $detailGroupe = $resultat->fetch();
    }
    return $detailGroupe;
}

/* Fonction pour modifier des détails des groupes */

function modifierGroupe($id, $nom, $identiteResponsable, $adressePostale, $nombrePersonnes, $nomPays, $hebergement) {
    $modifiaction = false;
    $pdo = gestionnaireDeConnexion();

    if ($pdo != false) {
        $id = $pdo->quote($id);
        $nom = $pdo->quote($nom);
        $identiteResponsable = $pdo->quote($identiteResponsable);
        $adressePostale = $pdo->quote($adressePostale);
        $nombrePersonnes = $pdo->quote($nombrePersonnes);
        $nomPays = $pdo->quote($nomPays);
        $hebergement = $pdo->quote($hebergement);

        $req = "update Groupe set nom=$nom, identiteResponsable=$identiteResponsable, adressePostale=$adressePostale, nombrePersonnes=$nombrePersonnes, nomPays=$nomPays, hebergement=$hebergement";
        $resultat = $pdo->exec($req);
        if (resultat == 1) {
            $modifiaction = true;
        }
    }
    return $modifiaction;
}

/* Fonction pour vérifier l'authentification */

function verification($user, $mdp) {
    $bool = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        /* Requete SQL en chaine de caractère */
        $sql = "Select count(*) as nb From compte"
                . " Where user=:user and mdp=:mdp"; //:paramètres nommés qui seront remplacé par des valeurs
        /* La méthode prepare transmet la requete SQL au SGBDR pour qu'elle soit analysée */
        $prep = $pdo->prepare($sql);
        /* La méthode bindParam ==> associer un nommé à une variable */
        $prep->bindParam(':user', $user, PDO::PARAM_STR);
        $prep->bindParam(':mdp', $mdp, PDO::PARAM_STR);
        /* La méthode execute ==> executer la requete */
        $prep->execute();
        /* La méthode fetch ==> lire le premier enregistrement du résultat de la requète */
        $resultat = $prep->fetch();
        /* Vérification de la bonne saisie de l'utilisateur */
        if ($resultat["nb"] == 1) {
            /* Le booléen prend la valeur TRUE signifiant que l'authentification est reussi */
            $bool = true;
        }
        return $bool;
    }
}

function listeAttribution() {
    $resultat = false;
    $pdo = gestionnaireDeConnexion(); /* Objet de connection */
    if ($pdo != false) {
        $req = "select A.*, E.nom as nomEtab, G.nom as nomGroupe"
                . " from Attribution A, Etablissement E, Groupe G"
                . " where A.idEtab= E.id"
                . " And A.idGroupe= G.id";
        $resultat = $pdo->query($req);
    }
    return $resultat;
}


?>